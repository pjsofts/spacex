# SpaceX Rocket Launch List

## Project Features and Structure

## Routing
use `routes.js` for `react-router v6` routing.

## State Management
`store/launchStore.js` and `store/settingsStore.js` are the two stores used.

## Layouts
`layous/MainLayout` contains the main layout which has a header.

Different Layouts can easily be added and used in the routing file for different views.

## Views
 `views/LaunchListView` is the main view.
 
 `views/BlankView` is a sample blank view.
 
for each page you can easily add a new view.
 
## Translations
`i18next` is used for translations.

`public/locales/en` and `public/locales/fa` are the tranlation folders.

The default (fallback) language is set by `REACT_APP_DEFAULT_LANG` variable in the `.env` file.

The theme `direction` has been configured to switch(rtl or ltr) when `fa` is selected.

To use translations or change the locale use the `useTranslation` hook.


## API calls
Api calls are written in the `src/api` folder. Currently we have `src/api/spacex.js`

For backend server URLs we use variables such as `REACT_APP_SPACEX_URL` in the `.env` file.


## Mock API
Mock is also available for when the backend is not ready.

mocks are in the `src/api/mocks` folder.

To enable mocks uncomment the `mockAxios(axios);` line in the `src/App.js` file.

## Themes
Two themes `lightTheme` and `darkTheme` have been configured to show the ability of switching themes.

`store/settingsStore` has a value `darkMode` to decide that.

## Components
General components are in the `src/components` folder.

`GlobalStyles.js` is the default Global Styles component.

`Loading.js` is a loading component for React's `Suspense` fallback.


 
## How build and Run the Project

### `npm start`

Runs the app in the development mode.


### `npm run build`

Builds the app for production to the `build` folder.




