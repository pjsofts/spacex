import React from "react";
import {Navigate} from 'react-router-dom';
import MainLayout from './layouts/MainLayout';
import LaunchListView from "./views/LaunchListView";
import BlankView from "./views/BlankView";

const routes = [
    {
        path: '/',
        element: <MainLayout/>,
        children: [
            {path: '/', element: <Navigate to="/launch/list"/>},
            {path: 'blank', element: <BlankView/>},
        ]
    },
    {
        path: 'launch',
        element: <MainLayout/>,
        children: [
            {path: 'list', element: <LaunchListView/>},
        ]
    }
];

export default routes;
