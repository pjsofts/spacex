import React, {useContext} from "react";
import {ThemeProvider} from '@material-ui/core/styles';
import {useRoutes} from 'react-router-dom';
import {LaunchStoreProvider} from "./store/launchStore";
import {settingsStore} from './store/settingsStore';
import GlobalStyles from "./components/GlobalStyles";
import routes from './routes';
import lightTheme from './themes/lightTheme';
import darkTheme from './themes/darkTheme';
import {useTranslation} from "react-i18next";
import {create} from 'jss';
import rtl from 'jss-rtl';
import {StylesProvider, jssPreset} from '@material-ui/core/styles';
import {useObserver} from 'mobx-react';

//eslint-disable-next-line
import mockAxios from "./api/mocks/mocksSetup";
//eslint-disable-next-line
import axios from 'axios';

const DEFAULT_LANG = process.env.REACT_APP_DEFAULT_LANG;

export default function App() {
    const routing = useRoutes(routes);

    //getting direction from language
    const {i18n} = useTranslation();
    const lang = i18n.language || DEFAULT_LANG;
    const direction = lang === "fa" ? "rtl" : "ltr";

    //setting dir attribute
    const htmlElement = document.getElementsByTagName("HTML")[0];
    htmlElement.setAttribute("dir", direction)

    //preparing jss
    const jss = direction === "rtl" ? create({plugins: [...jssPreset().plugins, rtl()]}) : undefined;

    const store = useContext(settingsStore);


    //Uncomment the below line to use mocks
    //mockAxios(axios);

    return useObserver(() => (
        <StylesProvider {...jss}>
            <ThemeProvider theme={store.darkMode ? darkTheme(direction) : lightTheme(direction)}>
                <LaunchStoreProvider>
                    <GlobalStyles/>
                    {routing}
                </LaunchStoreProvider>
            </ThemeProvider>
        </StylesProvider>
    ));
}
