import {createMuiTheme} from "@material-ui/core/styles";

const theme = (direction) => createMuiTheme({
    direction: direction,
    palette: {
        type: 'dark',
        primary: {
            main: "#cee7ff"
        },
        background: {
            paper: "#003c7f"
        },
    },
    typography: direction === "rtl" ? {
        fontFamily: 'IRANSans'
    } : {}
});


export default theme;