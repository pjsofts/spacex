import {createMuiTheme} from "@material-ui/core/styles";

const theme = (direction) => createMuiTheme({
    direction: direction,
    palette: {
        type: 'dark',
        primary: {
            main: "#d25e00"
        },
        background: {
            paper: "#282828"
        },
    },
    typography: direction === "rtl" ? {
        fontFamily: 'IRANSans'
    } : {}
});


export default theme;