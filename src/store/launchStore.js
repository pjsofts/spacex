import React, {createContext} from 'react';
import {useLocalStore} from 'mobx-react';
import {getLaunches} from "../api/spacex";

export const launchStore = createContext();
const PER_PAGE = 9;

export const LaunchStoreProvider = ({children}) => {
    const store = useLocalStore(() => ({
        search: "",
        upcoming: 0,
        launches: [],
        page: 1,
        loading: false,
        setSearch: (text) => {
            store.search = text;
            store.page = 1;
        },
        setUpcoming: async (upcoming) => {
            store.upcoming = upcoming
            store.loading = true;
            store.launches = await getLaunches(upcoming);
            store.loading = false;
            store.page = 1;
        },
        setPage: (page) => {
            store.page = page
        },
        get filteredLaunches() {
            return store.launches.filter(launch => launch.launch_site &&
                launch.launch_site.site_name_long &&
                launch.launch_site.site_name_long.toLowerCase().includes(store.search.toLowerCase()))
        },
        get sortedLaunches() {
            //reverse sort based on launch date
            return store.filteredLaunches.sort((launchA, launchB) => launchB.launch_date_unix - launchA.launch_date_unix)
        },
        get slicedLaunches() {
            const start = (store.page - 1) * PER_PAGE;
            const end = store.page * PER_PAGE;
            return store.sortedLaunches.slice(start, end)
        },
        get pageCount() {
            return Math.ceil(store.sortedLaunches.length / PER_PAGE)
        }
    }));
    return <launchStore.Provider value={store}>{children}</launchStore.Provider>
};

