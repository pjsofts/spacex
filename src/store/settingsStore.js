import React, {createContext} from 'react';
import {useLocalStore} from 'mobx-react';

export const settingsStore = createContext();

export const SettingsStoreProvider = ({children}) => {
    const store = useLocalStore(() => ({
        darkMode: true,
        toggleDarkMode() {
            store.darkMode = !store.darkMode;
        },
    }));
    return <settingsStore.Provider value={store}>{children}</settingsStore.Provider>
};

