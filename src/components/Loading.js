import React from 'react';
import {createMuiTheme, makeStyles, ThemeProvider} from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const DEFAULT_LANG = process.env.REACT_APP_DEFAULT_LANG;

const direction = DEFAULT_LANG === "fa" ? "rtl" : "ltr";

//separate theme before main theme has loaded
const theme = createMuiTheme({
    palette: {
        direction: direction,
        type: 'dark',
        primary: {
            main: "#d25e00"
        }
    }
});

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    }
}));

export default function Loading() {
    const classes = useStyles();
    return (
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <LinearProgress/>
            </div>
        </ThemeProvider>
    );
}