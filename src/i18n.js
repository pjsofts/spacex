import i18next from 'i18next';
import Backend from 'i18next-http-backend';
import {initReactI18next} from 'react-i18next';

const DEFAULT_LANG = process.env.REACT_APP_DEFAULT_LANG;

i18next
    .use(Backend)
    .use(initReactI18next)
    .init({
        fallbackLng: DEFAULT_LANG,
        debug: false,
        interpolation: {
            escapeValue: false,
        },
    });

export default i18next;