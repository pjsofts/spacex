import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import App from './App';
import './i18n';
import Loading from "./components/Loading";
import {SettingsStoreProvider} from "./store/settingsStore";

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Suspense fallback={<Loading/>}>
                <SettingsStoreProvider>
                    <App/>
                </SettingsStoreProvider>
            </Suspense>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);