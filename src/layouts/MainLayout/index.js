import React from 'react';
import {Outlet} from 'react-router-dom';
import {Container, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Header from './Header';

const useStyles = makeStyles((theme) => ({
    center: {
        display: 'flex',
        justifyContent: 'center'
    },
    container: {
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
    }
}));

export default function MainLayout({children}) {
    const classes = useStyles();
    return <Grid container className={classes.center}>
        <Grid item xs={12} md={9}>
            <Container className={classes.container}>
                <Header />
                <Outlet />
            </Container>
        </Grid>
    </Grid>
}