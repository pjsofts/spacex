import React, {useContext} from 'react';
import IconButton from '@material-ui/core/IconButton';
import BrightnessHighIcon from '@material-ui/icons/BrightnessHigh';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import {settingsStore} from "../../../store/settingsStore";
import {useObserver} from "mobx-react";

export default function IconButtons() {
    const store = useContext(settingsStore);

    const handleClick = () => {
        store.toggleDarkMode();
    }
    return useObserver(() => (
        <div>
            <IconButton aria-label="toggle-theme" onClick={handleClick}>
                {store.darkMode ? <BrightnessHighIcon/> :
                    <Brightness4Icon/>}
            </IconButton>
        </div>
    ));
}