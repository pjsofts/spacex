import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import LanguageMenu from './LanguageMenu';
import ToggleTheme from "./ToggleTheme";

const useStyles = makeStyles((theme) => ({
    header: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        marginBottom: '10px'
    },
    title: {
        flex: 1,
    },
    logo: {
        height: "40px",
    }
}));

export default function Header() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Toolbar className={classes.header}>
                <ToggleTheme/>
                <Typography
                    component="h2"
                    variant="body1"
                    color="textPrimary"
                    align="center"
                    noWrap
                    className={classes.title}
                >
                    <img src={"/assets/img/logo.svg"} alt={"SpaceX Logo"} className={classes.logo}/>
                </Typography>
                <LanguageMenu />
            </Toolbar>
        </React.Fragment>
    );
}