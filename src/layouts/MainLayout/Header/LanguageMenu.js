import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {useTranslation} from "react-i18next";

const DEFAULT_LANG = process.env.REACT_APP_DEFAULT_LANG;

export default function LanguageMenu() {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const {t, i18n} = useTranslation();

    const handleClose = (lang) => {
        console.log("changing language", lang);
        setAnchorEl(null);
        i18n.changeLanguage(lang);
    };

    return (
        <div>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                {t(i18n.language || DEFAULT_LANG)}
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={() => handleClose("en")}>{t("en")}</MenuItem>
                <MenuItem onClick={() => handleClose("fa")}>{t("fa")}</MenuItem>
            </Menu>
        </div>
    );
}