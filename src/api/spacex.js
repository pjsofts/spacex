import axios from 'axios';

const SPACEX_URL = process.env.REACT_APP_SPACEX_URL;

export const UPCOMING_URL = `${SPACEX_URL}/launches/upcoming`;
export const PAST_URL = `${SPACEX_URL}/launches/past`;

export const getLaunches = async (upcoming) => {
    const response = await axios.get(upcoming ? UPCOMING_URL : PAST_URL);
    return response.data;
}