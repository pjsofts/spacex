import {UPCOMING_URL, PAST_URL} from '../../spacex';
import {UPCOMING_MOCK_DATA, PAST_MOCK_DATA} from "./data";

export default function mockSpaceX(mock) {
    mock.onGet(UPCOMING_URL).reply(200, UPCOMING_MOCK_DATA);
    mock.onGet(PAST_URL).reply(200, PAST_MOCK_DATA);
}