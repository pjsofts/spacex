import MockAdapter from "axios-mock-adapter";
import mockSpaceX from "./mockSpacex";


export default function mockAxios(axios) {
    const mock = new MockAdapter(axios, { delayResponse: 300 });
    mockSpaceX(mock);
    return mock;
}
