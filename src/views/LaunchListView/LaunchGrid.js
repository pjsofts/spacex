import {Grid} from "@material-ui/core";
import LaunchCard from "./LaunchCard";
import {makeStyles} from "@material-ui/core/styles";
import {useContext} from "react";
import {useObserver} from 'mobx-react';
import {launchStore} from "../../store/launchStore";
import Loading from "../../components/Loading";


const useStyles = makeStyles((theme) => ({
    center: {
        display: 'flex',
        justifyContent: 'center',
    }
}));

export default function LaunchGrid() {
    const classes = useStyles();
    const store = useContext(launchStore);

    return useObserver(() => (<Grid container spacing={3}>
        {store.slicedLaunches.map((launch, index) => {
            return <Grid item sm={4} xs={12} className={classes.center} key={index}>
                <LaunchCard launch={launch}/>
            </Grid>
        })}
        {store.loading && <Grid item xs={12} className={classes.center}><Loading/></Grid>}
    </Grid>));
}
