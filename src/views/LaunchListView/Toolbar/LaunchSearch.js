import React, {useContext} from 'react';
import {useObserver} from 'mobx-react';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import {launchStore} from "../../../store/launchStore";
import {useTranslation} from "react-i18next";


const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        boxSizing: 'border-box',
        display: 'flex',
        alignItems: 'center',
        width: '100%'
    },
    input: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
}));

export default function LaunchSearch() {
    const classes = useStyles();
    const {t} = useTranslation();
    const store = useContext(launchStore);

    return useObserver(() => (
        <Paper component="form" className={classes.root} onSubmit={(e)=>{e.preventDefault()}}>
            <InputBase
                className={classes.input}
                placeholder={t("launch.search_sites")}
                value={store.search}
                onChange={(event) => {
                    store.setSearch(event.target.value)
                }}
                inputProps={{'aria-label': 'search launch sites'}}
            />
            <IconButton type="submit" className={classes.iconButton} aria-label="search">
                <SearchIcon/>
            </IconButton>
        </Paper>
    ));
}