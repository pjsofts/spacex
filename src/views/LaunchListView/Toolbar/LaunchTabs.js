import React, {useContext} from 'react';
import {useObserver} from 'mobx-react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {makeStyles} from '@material-ui/core/styles';
import {launchStore} from "../../../store/launchStore";
import {useTranslation} from "react-i18next";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
}));

export default function LaunchTabs() {
    const store = useContext(launchStore);
    const {t} = useTranslation();

    const handleChange = (event, newValue) => {
        store.setUpcoming(newValue);
    };

    const classes = useStyles();
    return useObserver(() => (
        <Paper className={classes.root}>
            <Tabs
                value={store.upcoming}
                indicatorColor="primary"
                textColor="primary"
                onChange={handleChange}
                centered={true}
                aria-label="disabled tabs example"
            >
                <Tab label={t("launch.past")}/>
                <Tab label={t("launch.upcoming")}/>
            </Tabs>
        </Paper>
    ));
}
