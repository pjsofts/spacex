import {Grid} from "@material-ui/core";
import Tabs from "./LaunchTabs";
import Search from "./LaunchSearch";
import Pagination from "./LaunchPagination";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    center: {
        display: 'flex',
        justifyContent: 'center'
    },
}));

export default function LaunchToolbar() {
    const classes = useStyles();
    return (<Grid container className={classes.center}>
        <Grid item md={6} sm={8} xs={12}>
            <Grid container spacing={1} className={classes.header}>
                <Grid item xs={12} className={classes.center}>
                    <Tabs/>
                </Grid>
                <Grid item xs={12} className={classes.center}>
                    <Search/>
                </Grid>
                <Grid item xs={12} className={classes.center}>
                    <Pagination/>
                </Grid>
            </Grid>
        </Grid>
    </Grid>)
}
