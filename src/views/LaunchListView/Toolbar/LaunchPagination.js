import React, {useContext} from 'react';
import {useObserver} from 'mobx-react';
import {makeStyles} from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import {launchStore} from "../../../store/launchStore";


const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export default function LaunchPagination() {
    const store = useContext(launchStore);

    const classes = useStyles();
    return useObserver(() => (
            <div className={classes.root}>
                <Pagination page={store.page}
                                  onChange={(event, page) => {
                                store.setPage(page)
                            }}
                                  count={store.pageCount}/>
            </div>

    ));
}