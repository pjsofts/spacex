import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import {format} from 'date-fns';
import {useTranslation} from "react-i18next";


const useStyles = makeStyles((theme) => ({
    card: {
        width: "100%"
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    avatar: {
        backgroundColor: theme.palette.primary.light,
        marginLeft: theme.direction === "rtl" && theme.spacing(2),
        marginRight: theme.direction === "rtl" && theme.spacing(-2),
    },
}));

export default function LaunchCard(props) {
    const classes = useStyles();
    const {t} = useTranslation();

    const {launch} = props;

    const image = () => {
        return (launch && launch.links && launch.links.flickr_images && launch.links.flickr_images.length > 0 && launch.links.flickr_images[0]) || (launch && launch.links && launch.links.mission_patch)
    }

    return (
        <Card className={classes.card}>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}
                            src={launch && launch.links && launch.links.mission_patch_small}/>
                }
                title={<Typography color={"primary"}>{launch.rocket.rocket_name}</Typography>}
                subheader={format(new Date(launch.launch_date_utc), "MMMM dd, yyyy")}
            />
            {image() && <CardMedia
                className={classes.media}
                image={image()}
                title={launch.mission_name}/>
            }
            <CardContent>
                <Typography variant="body1" color="textSecondary" component="p">
                    {launch.launch_site.site_name_long}
                </Typography>
                <Typography variant="body1" color="textSecondary" component="p">
                    <Typography variant="body2" color="primary" component="span">{t("launch.mission")}: </Typography>
                    {launch.mission_name}
                </Typography>
            </CardContent>
        </Card>
    );
}