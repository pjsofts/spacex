import React from 'react';
import {useTranslation} from 'react-i18next';
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Toolbar from "./Toolbar";

const useStyles = makeStyles((theme) => ({
    cover: {
        position: 'relative',
        marginBottom: theme.spacing(4),
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        height: "300px"
    },
    content: {
        position: 'relative',
        padding: theme.spacing(3),
        paddingBottom: theme.spacing(1),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(5),
            paddingBottom: theme.spacing(1),
        },
    },
    title: {
        fontSize: '1.7rem',
        [theme.breakpoints.up('md')]: {
            fontSize: '3rem',
        },
    }
}));

export default function LaunchCover() {
    const classes = useStyles();
    const {t} = useTranslation();

    return (
        <Paper className={classes.cover} style={{backgroundImage: `url('/assets/img/cover.jpg')`}}>
            {<img style={{display: 'none'}} src={"/assets/img/cover.jpg"} alt={"Earth"}/>}
            <Grid container>
                <Grid item xs={12}>
                    <div className={classes.content}>
                        <Typography component="h1" variant="h3" color="inherit" gutterBottom className={classes.title}>
                            {t('title').toUpperCase()}
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <Toolbar/>
                </Grid>
            </Grid>
        </Paper>
    );
}
