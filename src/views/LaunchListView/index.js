import {useContext, useEffect} from 'react';
import LaunchGrid from './LaunchGrid';
import LaunchCover from './LaunchCover';
import {launchStore} from "../../store/launchStore";

export default function Index() {
    const store = useContext(launchStore);

    useEffect(() => {
        store.setUpcoming(0)
    }, [store]);

    return (
        <>
            <LaunchCover/>
            <LaunchGrid/>
        </>
    );
}