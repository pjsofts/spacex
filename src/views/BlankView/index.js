import React from 'react';
import {Typography} from "@material-ui/core";

export default function BlankView() {
    return (
        <Typography variant={'h4'} component={'h1'} color={'primary'}>
            This is the exciting BLANK PAGE.
        </Typography>
    );
}